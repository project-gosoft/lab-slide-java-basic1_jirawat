package Lab;

import java.util.Scanner;

public class Lab_12 {
	public static void main(String[] args) {
		Art_No_1();
		Art_No_2();
		Art_No_3_4();
		Art_No_5_6();
		Art_No_7();

	}

//ทดลองเปรียบเทียบ String 2 String ว่าเป็นค่าเดียวกันหรือไม่
	public static void Art_No_1() {
		String myString_1 = "You and Me";
		String myString_2 = " you and me ";
		System.out.println("Article No.1 :--------------------------");
		System.out.print("Ans :");
		if (myString_1.equals(myString_2))
			System.out.println("True");
		else
			System.out.println("False");
	}

//ใช้คําสั่งค้นหาคําใน String และแสดงคําที่ค้นหาบนหน้าจอ
	public static void Art_No_2() {
		String myString_1 = "You and Me";
		String Search;
		Scanner sc = new Scanner(System.in);
		System.out.println("Article No.2 :--------------------------");
		System.out.println("TEXT : You and Me");
		System.out.print("input text: ");
		Search = sc.nextLine();
		System.out.println("Ans :" + myString_1.contains(Search));
	}

// ใช้คําสั่งหาความยาวของ String นั้น และแสดงค่าความยาว String
// ใช้คําสั่งตัดข้อความหรือตัด String ตําแหน่งที่ 1-4 ออก
	public static void Art_No_3_4() {
		String myString_1 = "You and Me";
		String myString_2 = " you and me ";
		System.out.println("Article No.3 :--------------------------");
		System.out.println("myString_1 length : " + myString_1.length());
		System.out.println("myString_2 length : " + myString_2.length());
		System.out.println("Article No.4 :-------------");
		System.out.println("Ans :" + myString_1.substring(4)); // เอาข้อความตั้งแต่ 1-4 ออก แต่ถ้าตั้งค่าว่า (4,10)
																// จะได้ค่าว่า
		// tStrin

	}

// ใช้คําสั่งตัดช่องว่างของประโยค
// ใช้คําสั่งเปลี่ยน String เป็นพิมพ์ ใหญ่ทั้งหมด
	public static void Art_No_5_6() {
		String myString_2 = " you and me ";
		System.out.println("Article No.5 :--------------------------");
		System.out.println(myString_2 + ": length: " + myString_2.length());
		String trimString = myString_2.trim();
		System.out.println(trimString + "  : length: " + trimString.length());
		System.out.println("Article No.6 :--------------------------");
		System.out.println("Ans :" + trimString.toUpperCase());
	}

// ใช้คําสั่งเปลี่ยน String2 เป็นพิมพ์ ใหญ่ทั้งหมด และไม่มีช่องว่างซ้ายขวาด้วยการเขียน code แค่บรรทัดเดียว (ใช้ Chaining นั่นเอง)
	public static void Art_No_7() {

		String myString_2 = " you and me ";
		System.out.println("Article No.7 :--------------------------");
		System.out.println(setCapital(myString_2));

	}

	static String setCapital(String name) {
		return name.substring(0, 11).toUpperCase().trim();
	}

}
