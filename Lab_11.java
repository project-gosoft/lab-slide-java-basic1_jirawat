package Lab;

import java.util.Scanner;

public class Lab_11 {
	public static void main(String[] args) {
		Q1();
		Q2();

	}

	public static void Q1() {
		System.out.println("Welcome to Calculator Program");
		Scanner reader = new Scanner(System.in);
		int x;
		int y;

		System.out.print("x = ");
		x = reader.nextInt();
		System.out.print("y = ");
		y = reader.nextInt();

		System.out.println("x + y = " + sum(x, y));

	}

	private static int sum(int a, int b) {
		return a + b;
	}

	public static void Q2() {
		String greeting = "No Return";
		sayHello(greeting);
	}

	public static void sayHello(String word) {
		System.out.println(word);
	}
}
