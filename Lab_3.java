package Lab;

public class Lab_3 {
	public static void main(String[] args) {
	  int  i = 20 ; 
	    float a = 1.1f;
	    float b = 1.2f;
	    
	    char c = 'c';
	    char d = 'd';
	    
	    //เรียก i++ 5 ครั้ง กับ i-- 5 ครั้ง
	    i++ ; i++ ; i++ ; i++ ; i++ ;
	    --i ; --i ; --i ; --i ; --i ;
	    
	    //เรียกผลลัพย์ค่า I
	    System.out.println(i);
	    System.out.println("**********");
	    
	    //การใช้งานค่า && , ||
	    if ((a == 1.1f) &&  (b == 1.2f))
	    System.out.println("a is 5.5f AND b is 5.6f");
	    
	    System.out.println("**********");
	    if((c == 'c') ||  (d == 'g'))
	        System.out.println("c is c OR d is g");
	}
}
