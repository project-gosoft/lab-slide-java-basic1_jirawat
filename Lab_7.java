package Lab;

import java.util.Scanner;

public class Lab_7 {
	public static void main(String[] args) {
		Q1();
		System.out.println(" ");
		Q2();
	}

	public static void Q1() {
		for (int i = 20; i >= 1; i--) {
			System.out.println("Loop i :" + i);
		}
		System.out.println("End of loop");
		// *******************************************
		System.out.println(" ");
		//*******************************************
		int i = 20;
		do {
			System.out.println("Value i : " + i);
			i--;
		} while (i >= 1);
		System.out.println("End of loop");
	}

	public static void Q2() {
		Scanner reader = new Scanner(System.in);
		int number;
		System.out.println("Determine odd/even program");
		do {
			System.out.print("Enter odd number to exit loop: ");
			number = reader.nextInt();

			if (number % 2 == 0) {
				System.out.println("You entered " + number + ", it's even.");
				System.out.println(" ");
			} else {
				System.out.println("You entered " + number + ", it's odd.");
			}

		} while (number % 2 == 0);

		System.out.println("Exited loop.");
	}
}
