package Lab;

public class Lab_13 {
	public static void main(String[] args) {
		int[][] Array = { 
				{ 1, 2, 3 }, 
				{ 4, 5, 6, 7 },
				{ 8, 9 } };
		for (int[] row : Array) {
			for (int element : row) {
				System.out.print(" "+element);
			}
		}
		int sum = Array[0][1]+Array[0][2]+Array[1][2]+Array[1][3]+Array[2][0]+Array[2][1];
		System.out.println("");
		System.out.println("Ans :"+ sum);
	}
}
