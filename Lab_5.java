package Lab;
import java.util.Scanner;
public class Lab_5 {
	public static void main(String[] args) {
		   Scanner num = new Scanner(System.in);
			int number;
			System.out.print("Enter your number: ");
			number = num.nextInt();
		   switch (number/10) {
		   case 10:
		   case 9:
		   case 8:
		     System.out.println("Grade : A"); break;
		   case 7:
		     System.out.println("Grade : B"); break;
		   case 6:
		     System.out.println("Grade : C"); break;
		   case 5:
			 System.out.println("Grade : D"); break;
		   case 4:
			 System.out.println("Grade : F"); break;
		   default:
		     System.out.println("Grade : E");
		   }
		 }
}
