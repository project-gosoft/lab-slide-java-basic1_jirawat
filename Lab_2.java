package Lab;
public class Lab_2 {
	public static void main(String[] args) {
    	bark();
    }
    public static void bark() {
    	String dogName = "Black";
		System.out.println("the Dog name " + dogName +"bark");
		
		float Float = 5.5f;
		int Int1 = 5;
		double Double = 5.5;
		char Char = 'Z';
		
		//float -> int
		int Int = (int) Float;
		System.out.println("float -> int value " + Int);
		//int -> float		
		float Float1 = Int1;
		System.out.println("int -> float value " + Float1);		
		//double -> float
		float Float2 =(float) Double;
		System.out.println("double -> float value " + Float2);
		//char -> int
		int  Int3 = (int) Char;
		System.out.println("char -> int value " + Int3);
	
		
		final String hello = "Hello";
		//hello = "world";
	    System.out.println("Age: " + hello);
	    //เมื่อทำการ run โดยใช้ hello = "world"; จะมีการแจ้งเตือน error
    }
}

